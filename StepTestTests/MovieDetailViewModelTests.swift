import XCTest
import Combine

@testable import StepTest
class MovieDetail: XCTestCase {
    
    private var sut: MovieDetailViewModel!
    private var item: MovieDetailItem!
    private var cancellables = Set<AnyCancellable>()
    
    override func setUp() {
        sut = MovieDetailViewModel(
            network: .init(environment: .themoviedb),
            storage: FavouritesRepository(),
            id: 878539
        )
        
        item = MovieDetailItem(
            id: 878539,
            title: "Testing",
            overview: nil,
            releaseDate: Date(),
            popularity: 1,
            posterPath: nil
        )
    }
    
    override func tearDown() {
        cancellables = []
        super.tearDown()
    }
    
    func test_loadEvent_shouldPopulateItem() {
        let expectation = XCTestExpectation(description: "load item subject sent")

        sut.item
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if value.id == self.item.id { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.load)

        wait(for: [expectation], timeout: 30)
    }
}
