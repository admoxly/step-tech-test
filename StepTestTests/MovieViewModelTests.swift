import XCTest
import Combine

@testable import StepTest
class MovieViewModelTests: XCTestCase {
    
    private var sut: MovieViewModel!
    private var item: MovieListItem!
    private var cancellables = Set<AnyCancellable>()
    
    override func setUp() {
        sut = MovieViewModel(
            network: .init(environment: .themoviedb),
            storage: FavouritesRepository()
        )
        
        item = MovieListItem(
            id: 1,
            title: "Testing",
            releaseDate: Date(),
            posterPath: nil
        )
    }
    
    override func tearDown() {
        cancellables = []
        super.tearDown()
    }
    
    func test_loadEvent_shouldPopulateItems() {
        let expectation = XCTestExpectation(description: "load items subject sent")

        sut.items
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if !value.isEmpty { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.load)

        wait(for: [expectation], timeout: 5)
    }
    
    func test_searchEvent_shouldStartSearching() {
        let expectation = XCTestExpectation(description: "isSearching subject (true) sent")

        sut.isSearching
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if value { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.searchQueryChanged("test string"))

        wait(for: [expectation], timeout: 5)
    }
    
    func test_searchEvent_shouldEndSearching_givenEmptyString() {
        let expectation = XCTestExpectation(description: "isSearching subject (false) sent")

        sut.isSearching
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if value == false { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.searchQueryChanged(""))

        wait(for: [expectation], timeout: 5)
    }
    
    func test_searchEvent_shouldEndSearching_givenNilValue() {
        let expectation = XCTestExpectation(description: "isSearching subject (false) sent")

        sut.isSearching
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if value == false { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.searchQueryChanged(nil))

        wait(for: [expectation], timeout: 5)
    }
    
    func test_searchEndedEvent_shouldPopulateItems() {
        let expectation = XCTestExpectation(description: "items subject sent")

        sut.items
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if !value.isEmpty { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.searchQueryEnded)

        wait(for: [expectation], timeout: 5)
    }
    
    func test_itemSelectedEvent_shouldSendSelectedIndex() {
        let expectation = XCTestExpectation(description: "didSelectItem subject sent")

        sut.didSelectItem
        .sink(
            receiveCompletion: { completion in
                guard case .finished = completion else { return }
            },
            receiveValue: { value in
                if value == self.item.id { expectation.fulfill() }
            }
        )
        .store(in: &cancellables)
        
        sut.send(.itemSelected(item.id))

        wait(for: [expectation], timeout: 5)
    }
}
