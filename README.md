# Step Technical Test
Step iOS technical test project using MVVM-C architecture, UIKit and Combine.

## Includes

* MVVM-C architecure
* Combine
* UIKit
* Dependency injection
* View state management via Combine chains
* Light and dark mode support
* Localised strings

## Dependencies

Dependency manager used is Swift Package Manager.

### Kingfisher 

A pure-Swift library for downloading and caching images from a url.

Given more time, this ideally would have been something to replace with a custom image loading library rather than pulling in a third party dependancy.

However, due to Kingfisher being used very commonly in projects, and to build this within time frame it was used in the list view and detail view to load in images.

## Choices

### Networking 

Networking layer is written in Combine, with the ability to handle and extend from JSON if required, such as adding an XML transformer e.g transform: .validated(.xml)

### User Interface

The user interfaces are built using UIKit with iOS 13 support. List screen and detail screen both use diffable data sources driven by the state provided through the ViewModel via the Combine chains.

Where possible, strings used within the user interface make use of localised strings files (with only English entered currently).

### Storage

Storage within the application for favourites is done with a simple repository wrapper around UserDefaults. If this were to be built upon more or scales to production where more data is needed to be stored, I would recommend this to be refactored into using a full local persistence storage such as CoreData or GRDB.

## Further Improvements

Given the time and scaling the application further, improvements that could be made would be testing the networking layer via stubbing / mocking the responses. 

During testing I found TheMovieDB API returned down an empty string for release dates rather than nil value. Stubbing out the responses would we know for sure the response structures to decode into would be correct at that point in time.