public extension L10n.Screen {
    
    enum Home {
        public static var title: String {
            localizedString("screen.home.title", comment: "Home Title")
        }
        
        public static var sectionTitle: String {
            localizedString("screen.home.section-title", comment: "Home Section Title")
        }
        
        public static var released: String {
            localizedString("screen.home.released", comment: "Released (context date)")
        }
        
        public static var failureMessagePrompt: String {
            localizedString("screen.home.failure-message-prompt", comment: "Failure Message Prompt")
        }
    }
}
