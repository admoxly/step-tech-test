public extension L10n.Screen {
    
    enum Search {
        public static var sectionTitle: String {
            localizedString("screen.search.section-title", comment: "Search Section Title")
        }
        
        public static var searchPlaceholder: String {
            localizedString("screen.search.search-placeholder", comment: "Search Placeholder")
        }
    }
}
