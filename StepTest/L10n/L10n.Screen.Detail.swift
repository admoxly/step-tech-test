public extension L10n.Screen {
    
    enum Detail {
        public static var released: String {
            localizedString("screen.detail.released", comment: "Released (context date)")
        }
        
        public static var popularityRating: String {
            localizedString("screen.detail.popularity-rating", comment: "Popularity Rating")
        }
        
        public static var failureMessagePrompt: String {
            localizedString("screen.detail.failure-message-prompt", comment: "Failure Message Prompt")
        }
    }
}
