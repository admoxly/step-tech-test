import Foundation

public enum L10n {
    public enum Screen {}
    
    public enum Common {
        public static var unknown: String {
            localizedString("common.unknown", comment: "Unknown")
        }
    }
    
    public enum Action {
        public static var errorOccurred: String {
            localizedString("action.error-occurred", comment: "Error Occurred")
        }
        
        public static var cancel: String {
            localizedString("action.cancel", comment: "Cancel")
        }
        
        public static var tryAgain: String {
            localizedString("action.try-again", comment: "Try Again")
        }
    }
}

public func localizedString(_ key: String, comment: String) -> String {
    NSLocalizedString(key, bundle: Bundle.main, comment: comment)
}
