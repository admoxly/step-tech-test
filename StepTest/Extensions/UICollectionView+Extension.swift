import UIKit

extension UICollectionView {
    
    func register<C>(_ reusableViewType: C.Type) where C: UICollectionReusableView {
        let identifer = reusableViewType.identifier
        register(UINib(nibName: identifer, bundle: nil), forSupplementaryViewOfKind: identifer, withReuseIdentifier: identifer)
    }
    
    func register<C>(_ cellType: C.Type) where C: UICollectionViewCell {
        let identifer = cellType.identifier
        register(UINib(nibName: identifer, bundle: nil), forCellWithReuseIdentifier: identifer)
    }
        
    func dequeue<C>(_ cellType: C.Type, at indexPath: IndexPath) -> C where C: UICollectionViewCell {
        let identifer = cellType.identifier
        let cell = dequeueReusableCell(withReuseIdentifier: identifer, for: indexPath) as! C
        
        return cell
    }
}
