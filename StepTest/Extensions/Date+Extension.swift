import Foundation

extension Date {
    
    func toFormatted(dateFormat: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = dateFormat.dateFormatterFormat
        
        return dateFormatter.string(from: self)
    }
    
    enum DateFormat {
        case short
        case long
        
        var dateFormatterFormat: String {
            switch self {
            case .short: return "MMM d, yyyy"
            case .long: return "EEEE, MMM d, yyyy"
            }
        }
    }
}
