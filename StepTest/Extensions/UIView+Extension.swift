import UIKit

extension UIView {
    func applyCornerRadius(_ radius: CGFloat = 8, forCorners cornerMask: CACornerMask = .all) {
        layer.cornerRadius = radius
        layer.maskedCorners = cornerMask
        layer.masksToBounds = true
    }
    
    func applyCircleCornerRadius() {
        applyCornerRadius(frame.size.width / 2)
    }
}

extension CACornerMask {
    
    static var all: CACornerMask {
        return [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
    }
    
    static var bottom: CACornerMask {
        return [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
}
