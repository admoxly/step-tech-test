import UIKit
import Combine

final class AppCoordinator: Coordinator {
    
    private var cancellables = Set<AnyCancellable>()
    
    private let window: UIWindow
    private let environment: Environment
    private let network: Network
    
    private lazy var movieViewModel = MovieViewModel(
        network: network,
        storage: FavouritesRepository()
    )
    
    var navigationController: UINavigationController

    init(
        navigationController: UINavigationController,
        window: UIWindow,
        environment: Environment
    ) {
        self.navigationController = navigationController
        self.window = window
        self.environment = environment
        self.network = Network(environment: environment)
    }

    func start() {
        movieViewModel.didSelectItem.sink { [weak self] item in
            self?.navigateToItem(with: item)
        }.store(in: &cancellables)
        
        let viewController = HomeViewController(movieViewModel: movieViewModel, coordinator: self)
        navigationController.setViewControllers([viewController], animated: true)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    private func navigateToItem(with id: Int) {
        let viewModel = MovieDetailViewModel(network: network, storage: FavouritesRepository(), id: id)
        let viewController = MovieDetailViewController(viewModel: viewModel)
        
        viewController.completionHandler = { value in
            self.movieViewModel.hasUpdated.send(value)
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
}
