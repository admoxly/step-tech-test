import Foundation

/// The HTTP method of an API request
enum HTTPMethod {
    case get
    
    var type: String {
        switch self {
        case .get: return "GET"
        }
    }
}

