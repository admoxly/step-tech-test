import Foundation

/// The HTTP error of an API request
enum HTTPError: Error {
    /// 401 Status code
    case unauthorised
    /// 404 Status code
    case notFound
    /// Invalid URL
    case invalidURL
    /// Invalid Request
    case invalidRequest(URLError)
    /// Invalid Response
    case invalidResponse
    /// Server Error
    case serverError(ErrorResponse)
    /// Internal Decoding Failure
    case decodingFailure(Error)
    /// Unable to get success or error decoded
    case unknown
}
