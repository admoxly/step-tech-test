import Foundation
import Combine

struct HTTPRequestTransform<Value> {
    /// A function that transforms the server response in to a new representation
    var run: (Data, HTTPURLResponse) -> AnyPublisher<Value, HTTPError>
}

extension HTTPRequestTransform {
    /// Verifies the response status code and decodes API errors
    /// - Parameter wrapped: The transform to run if the response is successful
    static func validated(_ wrapped: HTTPRequestTransform) -> Self {
        .decodeError(.init { data, response in
            switch response.statusCode {
            case 200: return wrapped.run(data, response)
            case 401: return Fail(error: HTTPError.unauthorised).eraseToAnyPublisher()
            case 404: return Fail(error: HTTPError.notFound).eraseToAnyPublisher()
            default: break
            }
            
            return Fail(error: HTTPError.serverError(ErrorResponse(
                statusCode: response.statusCode,
                statusMessage: "Unsupported HTTP Error Code"
            ))).eraseToAnyPublisher()
        })
    }
    
    /// Attempts to decode an API error from the response
    /// - Parameter wrapped: The transform to run if no error is found
    static private func decodeError(_ wrapped: HTTPRequestTransform) -> Self {
        .init { data, response in
            if let mapped = try? JSONDecoder().decode(ErrorResponse.self, from: data) {
                return Fail(error: HTTPError.serverError(mapped)).eraseToAnyPublisher()
            }
            
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
               let message = json["status_message"] as? String {
                return Fail(error: HTTPError.serverError(ErrorResponse(
                    statusCode: response.statusCode,
                    statusMessage: message
                ))).eraseToAnyPublisher()
            }
            
            return wrapped.run(data, response)
        }
    }
}

extension HTTPRequestTransform where Value: Decodable {
    /// Attempts to decode the response as JSON
    static var json: Self {
        .init { data, _ in
            let decoder = JSONDecoder.default

            do {
                let value = try decoder.decode(Value.self, from: data)
                return Just(value).setFailureType(to: HTTPError.self).eraseToAnyPublisher()
            } catch {
                return Fail(error: HTTPError.decodingFailure(error)).eraseToAnyPublisher()
            }
        }
    }
}
