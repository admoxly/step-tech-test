import Foundation

struct HTTPRequest<Value> {
    var path: String
    var method: HTTPMethod
    var queryItems: [String: String]
    var transform: HTTPRequestTransform<Value>
    
    /// Creates a request.
    /// - Parameters:
    ///   - path: The API endpoint path
    ///   - method: The HTTP method of the request
    ///   - query: The query parameters for the request
    ///   - transform: Transforms the server response in to a new representation
    init(
        path: String,
        method: HTTPMethod,
        queryItems: [String: String] = [:],
        transform: HTTPRequestTransform<Value>
    ) {
        self.path = path
        self.method = method
        self.queryItems = queryItems
        self.transform = transform
    }
}

