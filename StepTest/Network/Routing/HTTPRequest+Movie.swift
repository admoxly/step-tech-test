import Foundation

extension HTTPRequest {
    /// GET /movie/now_playing
    static func getNowPlaying(page: Int) -> HTTPRequest<PaginatedResponse<MovieListResponse>> {
        .init(
            path: "/movie/now_playing",
            method: .get,
            queryItems: ["page": String(page)],
            transform: .validated(.json)
        )
    }
    
    /// GET /movie/{movie_id}
    static func getMovie(id: Int) -> HTTPRequest<MovieDetailResponse> {
        .init(
            path: "/movie/\(id)",
            method: .get,
            transform: .validated(.json)
        )
    }
}
