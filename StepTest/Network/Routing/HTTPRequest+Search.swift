import Foundation

extension HTTPRequest {
    /// GET /search/movie
    static func getSearchMovie(term: String) -> HTTPRequest<PaginatedResponse<MovieListResponse>> {
        .init(
            path: "/search/movie",
            method: .get,
            queryItems: ["query": term],
            transform: .validated(.json)
        )
    }
}
