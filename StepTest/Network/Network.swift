import Foundation
import Combine

final class Network {
    private let environment: Environment
    private let session: URLSession
    
    init(environment: Environment, configuration: URLSessionConfiguration = .default) {
        self.environment = environment
        self.session = URLSession(configuration: configuration)
    }

    /// Sends a request to the server
    /// - Parameters:
    ///   - request:  The request to be sent to the server
    /// - Returns: A publisher that emits a value when the request completes
    func request<Value: Decodable>(_ request: HTTPRequest<Value>) -> AnyPublisher<Value, HTTPError> {
        urlRequest(from: request).publisher
        .flatMap { self.session.dataTaskPublisher(for: $0).mapError { HTTPError.invalidRequest($0) } }
        .flatMap { self.process(request: request, data: $0.0, response: $0.1) }
        .eraseToAnyPublisher()
    }
    
    /// Attempts to create a `URLRequest` from a `HTTPRequest`
    /// - Parameters:
    ///   - request: The request definition to be converted in to a `URLRequest`
    /// - Returns: The created `URLRequest`
    private func urlRequest<Value>(from request: HTTPRequest<Value>) -> Result<URLRequest, HTTPError> {
        guard var url = URL(string: environment.baseURL) else {
            return .failure(HTTPError.invalidURL)
        }
        
        URLPathEncoder.resolve(url: &url, for: request.path)
        
        var urlRequest = URLRequest(url: url)
        
        URLParameterEncoder.encode(urlRequest: &urlRequest, with: request.queryItems)
        
        // Attach required API authentication query parameter
        URLParameterEncoder.encode(urlRequest: &urlRequest, with: ["api_key": environment.APIKey])

        urlRequest.httpMethod = request.method.type
        
        return .success(urlRequest)
    }
    
    /// Attempts to process the server response in to a new representation
    ///
    /// - Parameters:
    ///   - request: The request that was sent
    ///   - data: The response data
    ///   - response: The `URLResponse` describing the server response
    /// - Returns: A publisher that emits the transformed value
    private func process<Value>(request: HTTPRequest<Value>, data: Data, response: URLResponse) -> AnyPublisher<Value, HTTPError> {
        guard let response = response as? HTTPURLResponse else {
            return Fail(error: HTTPError.invalidResponse).eraseToAnyPublisher()
        }
        
        return request.transform.run(data, response)
    }
}
