import Foundation

struct Environment {
    
    let baseURL: String
    let mediaURL: String
    let APIKey: String
    
    init(baseURL: String,
         mediaURL: String,
         APIKey: String
    ) {
        self.baseURL = baseURL
        self.mediaURL = mediaURL
        self.APIKey = APIKey
    }
}
