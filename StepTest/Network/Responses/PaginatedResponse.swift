import Foundation

struct PaginatedResponse<T: Decodable> {
    let page: Int
    let totalPages: Int
    let totalResults: Int
    let results: [T]
}

extension PaginatedResponse: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case totalPages = "total_pages"
        case totalResults = "total_results"
        case results = "results"
    }
}
