import Foundation

struct ErrorResponse {
    let statusCode: Int
    let statusMessage: String
}

extension ErrorResponse: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case statusMessage = "status_message"
    }
}
