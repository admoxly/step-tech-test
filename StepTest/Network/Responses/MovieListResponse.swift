import Foundation

struct MovieListResponse: Decodable {
    let id: Int
    let title: String
    @EmptyDate var releaseDate: Date?
    let posterPath: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case releaseDate = "release_date"
        case posterPath = "poster_path"
    }
}
