import Foundation

struct MovieDetailResponse: Decodable {
    let id: Int
    let title: String
    let overview: String?
    @EmptyDate var releaseDate: Date?
    let popularity: Double
    let posterPath: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case overview = "overview"
        case releaseDate = "release_date"
        case popularity = "popularity"
        case posterPath = "poster_path"
    }
}
