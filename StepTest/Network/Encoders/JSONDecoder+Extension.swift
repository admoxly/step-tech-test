import Foundation

public extension JSONDecoder {
    static var `default`: JSONDecoder {
        let decoder = JSONDecoder()
        let formatter = DateFormatter.yyyyMMdd
        
        decoder.dateDecodingStrategy = .formatted(formatter)

        return decoder
    }
}
