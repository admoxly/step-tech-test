import Foundation

struct URLParameterEncoder {

    /// Encode parameters onto a URLRequest
    static func encode(urlRequest: inout URLRequest, with parameters: [String: String]) {
        guard !parameters.isEmpty, let url = urlRequest.url,
            var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
                return
        }
        
        var queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        queryItems.append(contentsOf: components.queryItems ?? [])

        components.queryItems = queryItems
        urlRequest.url = components.url
    }
}
