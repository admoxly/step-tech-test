import Foundation

struct URLPathEncoder {
    
    /// Resolve a URL by appending provided path component
    /// - Parameters:
    ///   - url: The URL to append path components against
    ///   - path: The path component to be appended
    static func resolve(url: inout URL, for path: String) {
        url = url.appendingPathComponent(path)
    }
}
