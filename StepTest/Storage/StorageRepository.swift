import Foundation

protocol StorageRepository: AnyObject {
    
    @discardableResult
    func create(entity: Int) -> Result<Int, StorageError>
    
    @discardableResult
    func exists(entity: Int) -> Result<Bool, StorageError>
    
    @discardableResult
    func delete(entity: Int) -> Result<Bool, StorageError>
}
