import Foundation

enum StorageError: Error {
    case unableToRetrieve
}
