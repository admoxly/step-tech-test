import Foundation

final class FavouritesRepository: StorageRepository {

    private lazy var userDefaults: UserDefaults = .standard
    private let cacheKey = "favourites"
    
    @discardableResult
    func create(entity: Int) -> Result<Int, StorageError> {
        var entities = userDefaults.stringArray(forKey: cacheKey) ?? []
        
        entities.append(String(entity))
        userDefaults.set(entities, forKey: cacheKey)

        return .success(entity)
    }
    
    @discardableResult
    func exists(entity: Int) -> Result<Bool, StorageError> {
        let entities = userDefaults.stringArray(forKey: cacheKey) ?? []
        return .success(entities.contains(where: { $0 == String(entity) }))
    }
    
    @discardableResult
    func delete(entity: Int) -> Result<Bool, StorageError> {
        guard var entities = userDefaults.stringArray(forKey: cacheKey) else {
            return .failure(.unableToRetrieve)
        }
        
        entities.removeAll(where: { $0 == String(entity) })
        
        userDefaults.set(entities, forKey: cacheKey)
        
        return .success(true)
    }
}
