extension Environment {
    
    static var themoviedb: Self {
        .init(
            baseURL: "https://api.themoviedb.org/3",
            mediaURL: "http://image.tmdb.org/t/p/w500",
            APIKey: "8071bf58fd3161e9bdcbbe1f2c53701b"
        )
    }
}
