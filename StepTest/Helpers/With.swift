/// Modifies a value using a function.
/// - Parameters:
///   - value: Any value.
///   - f: A function used to modify the value. The function will receive a reference to the value.
/// - Returns: The modified value.
public func with<T>(_ value: T, _ f: (inout T) -> Void) -> T { // swiftlint:disable:this identifier_name
    var value = value
    f(&value)
    return value
}
