import Foundation

/// This has to be a custom decoder due to movie db sending down release date sometimes as `"release_date": ""` rather than null or a date string.
///
///See: `https://api.themoviedb.org/3/search/movie?api_key=8071bf58fd3161e9bdcbbe1f2c53701b&query=Wol`
///
/// For example (14th entry - "Teen Wolf: The Movie")
@propertyWrapper
struct EmptyDate {
    let wrappedValue: Date?
}

extension EmptyDate: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        do {
            let date = try container.decode(Date.self)
            self.wrappedValue = date
        } catch {
            self.wrappedValue = nil
        }
    }
}
