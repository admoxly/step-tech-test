enum MovieListViewEvent {
    case load
    case favourite(MovieListItem)
    case searchQueryChanged(String?)
    case searchQueryEnded
    case itemSelected(Int)
}
