import UIKit
import Kingfisher

class MoviePosterCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var favouriteButton: UIButton!
    @IBOutlet private weak var movieTitleLabel: UILabel!
    @IBOutlet private weak var backdropImageView: UIImageView!
    @IBOutlet private weak var releaseDateLabel: UILabel!
    
    var favouriteButtonTapHandler: ((MovieListItem) -> Void)?
    
    var movie: MovieListItem? {
        didSet {
            guard let movie = movie else { return }
            
            let releaseDate = movie.releaseDate?.toFormatted(dateFormat: .short) ?? L10n.Common.unknown
            
            releaseDateLabel.text = "\(L10n.Screen.Home.released): \(releaseDate)"
            
            movieTitleLabel.text = movie.title
            backdropImageView.kf.setImage(
                with: movie.posterURL,
                placeholder: UIImage(named: "PosterPlaceholder")
            )
            
            favouriteButton.setImage(UIImage(
                systemName: movie.isFavourited ? "heart.fill" : "heart"),
                for: .normal
            )
            
            favouriteButton.setTitle(
                movie.isFavourited ? "Favourited" : "Favourite",
                for: .normal
            )
            
            layoutIfNeeded()
        }
    }
    
    override func layoutSubviews() {
        backdropImageView.applyCornerRadius()
        favouriteButton.applyCornerRadius()
    }
    
    override func prepareForReuse() {
        movieTitleLabel.text = nil
        backdropImageView.image = nil
        releaseDateLabel.text = nil
    }
    
    @IBAction private func favouriteButtonTapped(_ sender: UIButton) {
        guard let movie = movie else { return }
        favouriteButtonTapHandler?(movie)
        
        favouriteButton.setImage(UIImage(systemName: movie.isFavourited ? "heart.fill" : "heart"), for: .normal)
        
        layoutIfNeeded()
    }
}
