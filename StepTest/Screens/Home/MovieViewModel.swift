import Foundation
import Combine

final class MovieViewModel {
    
    private let network: Network
    private let storage: StorageRepository
    
    enum FailureType {
        case home
        case search(String)
    }
    
    let items = CurrentValueSubject<[MovieListItem], Never>([])
    let hasUpdated = PassthroughSubject<Int, Never>()
    let isSearching = PassthroughSubject<Bool, Never>()
    let isLoading = CurrentValueSubject<Bool, Never>(false)
    let hasFailed = PassthroughSubject<FailureType, Never>()
    let didSelectItem = PassthroughSubject<Int, Never>()

    private var page = 1
    
    private var cancellables = Set<AnyCancellable>()

    init(network: Network, storage: StorageRepository) {
        self.network = network
        self.storage = storage
    }
    
    public func send(_ event: MovieListViewEvent) {
        switch event {
        case .load:
            retrieveItems(resettingResults: false)
        case let .favourite(item):
            if case let .success(value) = storage.exists(entity: item.id) {
                if value {
                    storage.delete(entity: item.id)
                } else {
                    storage.create(entity: item.id)
                }
                
                hasUpdated.send(item.id)
            }
        case let .searchQueryChanged(query):
            guard let query = query, !query.isEmpty else {
                isSearching.send(false)
                return
            }
            
            isSearching.send(true)
            
            retrieveSearch(term: query)
        case .searchQueryEnded:
            retrieveItems(resettingResults: true)
        case let .itemSelected(index):
            didSelectItem.send(index)
        }
    }
    
    private func retrieveItems(resettingResults: Bool) {
        isLoading.send(true)
        
        if resettingResults { page = 1 }
        
        network
            .request(.getNowPlaying(page: page))
            .sink(receiveCompletion: { [weak self] value in
                switch value {
                case .finished: self?.isLoading.send(false)
                case .failure:
                    self?.isLoading.send(false)
                    self?.hasFailed.send(.home)
                }
            }, receiveValue: {  [weak self] value in
                // Increment the page by one for any additionally paginated pages
                self?.page = value.page + 1
                
                self?.items.send(value.results.map {
                    MovieListItem(
                        id: $0.id,
                        title: $0.title,
                        releaseDate: $0.releaseDate,
                        posterPath: $0.posterPath
                    )
                })
            })
            .store(in: &cancellables)
    }
    
    private func retrieveSearch(term: String) {
        isLoading.send(true)
        
        network
            .request(.getSearchMovie(term: term))
            .map(\.results)
            .output(in: 0...19)
            .sink(receiveCompletion: { [weak self] value in
                switch value {
                case .finished: self?.isLoading.send(false)
                case .failure:
                    self?.isLoading.send(false)
                    self?.hasFailed.send(.search(term))
                }
            }, receiveValue: { [weak self] values in
                guard let self = self else { return }
                
                self.items.send(self.map(values))
            })
            .store(in: &cancellables)
    }
                  
    private func map(_ items: [MovieListResponse]) -> [MovieListItem] {
        return items.map {
            MovieListItem(
                id: $0.id,
                title: $0.title,
                releaseDate: $0.releaseDate,
                posterPath: $0.posterPath
            )
        }
    }
}
