import UIKit
import Combine

class HomeViewController: UIViewController {
    
    private typealias DataSource = UICollectionViewDiffableDataSource<Section, MovieListItem>

    private enum Section {
        case main
    }
    
    private let movieViewModel: MovieViewModel
    private let coordinator: Coordinator
    
    private var cancellables = Set<AnyCancellable>()
    private var isFirstSnapshot = false
    private var dataSource: DataSource?
    private var snapshot = NSDiffableDataSourceSnapshot<Section, MovieListItem>()
    private var isSearching: Bool = false
    
    private lazy var searchController = with(UISearchController(searchResultsController: nil)) {
        $0.searchBar.placeholder = L10n.Screen.Search.searchPlaceholder
        $0.searchResultsUpdater = self
        $0.delegate = self
        $0.obscuresBackgroundDuringPresentation = false
    }

    private lazy var activityIndicatorView = with(UIActivityIndicatorView()) {
        $0.hidesWhenStopped = true
        $0.style = .large
        $0.color = .gray
    }

    private lazy var collectionView = with(UICollectionView(
        frame: .zero,
        collectionViewLayout: makeCollectionViewLayout()
    )) {
        $0.delegate = self
        $0.backgroundColor = .systemBackground
    }
    
    
    private lazy var emptyStateLabel = with(UILabel()) {
        $0.font = .preferredFont(forTextStyle: .headline)
        $0.textColor = .systemGray
        $0.text = "No results found. Please try again."
        $0.isHidden = true
        $0.textAlignment = .center
    }
    
    public init(movieViewModel: MovieViewModel, coordinator: Coordinator) {
        self.movieViewModel = movieViewModel
        self.coordinator = coordinator

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureDataSource()
        setupNavigationView()
        setupViews()
        bindToViewModel()
    }
    
    private func setupNavigationView() {
        title = L10n.Screen.Home.title

        navigationItem.searchController = searchController
        navigationItem.largeTitleDisplayMode = .never
        
        view.backgroundColor = .systemBackground
    }
    
    private func setupViews() {
        view.addSubview(collectionView)
        view.addSubview(emptyStateLabel)
        view.addSubview(activityIndicatorView)
        
        emptyStateLabel.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            
            emptyStateLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 10),
            emptyStateLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            emptyStateLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    private func bindToViewModel() {
        movieViewModel.items.sink { [weak self] items in
            self?.updateSnapshot(with: items)
        }.store(in: &cancellables)
        
        movieViewModel.isLoading.sink { [weak self] in
            self?.configureLoadingState($0)
        }.store(in: &cancellables)
        
        movieViewModel.hasFailed.sink { [weak self] value in
            self?.handleFailure(value)
        }.store(in: &cancellables)
        
        movieViewModel.isSearching.sink { [weak self] value in
            guard let self = self else { return }
            
            self.isSearching = value
            
            self.snapshot.deleteAllItems()
            self.dataSource?.apply(self.snapshot, animatingDifferences: true)
        }.store(in: &cancellables)

        movieViewModel.hasUpdated.sink { [weak self] value in
            self?.updateSnapshot(with: value)
        }.store(in: &cancellables)
        
        movieViewModel.send(.load)
    }
    
    private func configureLoadingState(_ isLoading: Bool) {
        DispatchQueue.main.async  {
            if isLoading {
                self.activityIndicatorView.startAnimating()
            } else {
                self.activityIndicatorView.stopAnimating()
            }
        }
    }
    
    private func handleFailure(_ type: MovieViewModel.FailureType) {
        let alertController = UIAlertController(
            title: L10n.Action.errorOccurred,
            message: L10n.Screen.Home.failureMessagePrompt,
            preferredStyle: .alert
        )

        alertController.addAction(.init(title: L10n.Action.cancel, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(.init(title: L10n.Action.tryAgain, style: .default) { [weak self] _ in
            switch type {
            case .home: self?.movieViewModel.send(.load)
            case let .search(term): self?.movieViewModel.send(.searchQueryChanged(term))
            }
        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: Datasource -
    
    private func configureDataSource() {
        collectionView.register(HeaderTitleCollectionReusableView.self)
        collectionView.register(MoviePosterCollectionViewCell.self)
        
        dataSource = .init(
            collectionView: collectionView
        ) { collectionView, indexPath, item -> UICollectionViewCell? in
           guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: MoviePosterCollectionViewCell.identifier,
                for: indexPath) as? MoviePosterCollectionViewCell else {
                    return nil
                }
            
            cell.movie = item
            cell.favouriteButtonTapHandler = {
                self.movieViewModel.send(.favourite($0))
            }
            
            return cell
        }

        dataSource?.supplementaryViewProvider = { collectionView, kind, indexPath in            
            guard let cell = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: HeaderTitleCollectionReusableView.identifier,
                for: indexPath) as? HeaderTitleCollectionReusableView else {
                     return nil
                 }
             
            cell.title = self.isSearching ? L10n.Screen.Search.sectionTitle : L10n.Screen.Home.sectionTitle
            return cell
        }

        collectionView.dataSource = dataSource
    }
    
    // MARK: Snapshot -
    
    private func updateSnapshot(with identifier: Int) {
        if let dataSource = self.dataSource,
           let item = dataSource
            .snapshot()
            .itemIdentifiers
            .first(where: { $0.id == identifier }) {
            
            var snapshot = dataSource.snapshot()
            snapshot.reloadItems([item])

            dataSource.apply(snapshot)
        }
    }
    
    private func updateSnapshot(with items: [MovieListItem]) {
        let animated = !isFirstSnapshot
        
        isFirstSnapshot = !items.isEmpty
        
        DispatchQueue.main.async {
            if items.isEmpty {
                self.collectionView.isHidden = true
                self.emptyStateLabel.isHidden = false
            } else {
                self.collectionView.isHidden = false
                self.emptyStateLabel.isHidden = true
            }
        }

        if snapshot.sectionIdentifiers.isEmpty {
            snapshot.appendSections([.main])
        }

        snapshot.appendItems(items, toSection: .main)
        
        DispatchQueue.main.async {
            self.dataSource?.apply(self.snapshot, animatingDifferences: animated)
        }
    }
}

// MARK: - UICollectionViewDelegate

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let item = dataSource?.itemIdentifier(for: indexPath) else { return }
        
        movieViewModel.send(.itemSelected(item.id))
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard !isSearching, indexPath.row == snapshot.itemIdentifiers.count - 2 else { return }
        movieViewModel.send(.load)
    }
}

// MARK: - UISearchBarDelegate

extension HomeViewController: UISearchControllerDelegate {
    
    func didDismissSearchController(_ searchController: UISearchController) {
        movieViewModel.send(.searchQueryEnded)
    }
}

// MARK: - UISearchResultsUpdating

extension HomeViewController: UISearchResultsUpdating {
    
    public func updateSearchResults(for searchController: UISearchController) {
        let query = searchController.isActive ? searchController.searchBar.text ?? "" : nil
        movieViewModel.send(.searchQueryChanged(query))
    }
    
    override public func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)

        searchController.searchBar.isUserInteractionEnabled = !isEditing
        searchController.searchBar.searchTextField.isEnabled = !isEditing

        navigationController?.setToolbarHidden(!isEditing, animated: animated)
    }
}

// MARK: - UICollectionViewLayout

extension HomeViewController {
    
    private func makeCollectionViewLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            let itemSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(0.5),
                heightDimension: .fractionalHeight(1)
            )
            
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            item.contentInsets = NSDirectionalEdgeInsets(
                top: 5,
                leading: 10,
                bottom: 10,
                trailing: 10
            )
            
            let groupSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(0.4)
            )
            
            let group = NSCollectionLayoutGroup.horizontal(
                layoutSize: groupSize,
                subitem: item,
                count: 2
            )

            let section = NSCollectionLayoutSection(group: group)
            
            section.contentInsets = NSDirectionalEdgeInsets(
                top: 0,
                leading: 10,
                bottom: 0,
                trailing: 10
            )
            
            let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                                    heightDimension: .estimated(40))

            let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: headerSize,
                elementKind: HeaderTitleCollectionReusableView.identifier,
                alignment: .top
            )
            
            sectionHeader.contentInsets = NSDirectionalEdgeInsets(
                top: 0,
                leading: 10,
                bottom: 0,
                trailing: 10
            )

            section.boundarySupplementaryItems = [sectionHeader]
            
            return section
        }
    }
}
