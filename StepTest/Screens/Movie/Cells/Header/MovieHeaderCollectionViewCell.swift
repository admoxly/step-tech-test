import UIKit

class MovieHeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var backdropImageView: UIImageView!
    
    var url: URL? {
        didSet {
            backdropImageView.kf.setImage(
                with: url,
                placeholder: UIImage(named: "PosterPlaceholder")
            )
        }
    }
    
    override func layoutSubviews() {
        backdropImageView.applyCornerRadius(20, forCorners: .bottom)
    }
}
