import UIKit

class MovieInformationCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var releaseDateLabel: UILabel!
    @IBOutlet private weak var ratingLabel: UILabel!
    
    var movieInformation: MovieInformationItem? {
        didSet {
            let releaseDate = movieInformation?.releaseDate?.toFormatted(dateFormat: .long) ?? L10n.Common.unknown
            
            releaseDateLabel.text = "\(L10n.Screen.Detail.released): \(releaseDate)"
            
            guard let popularity = movieInformation?.popularity?.withCommas() else { return }
            ratingLabel.text = "\(L10n.Screen.Detail.popularityRating): \(popularity)"

            layoutIfNeeded()
        }
    }
}
