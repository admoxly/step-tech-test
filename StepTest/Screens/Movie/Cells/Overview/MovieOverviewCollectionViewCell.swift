import UIKit

class MovieOverviewCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var overviewLabel: UILabel!
    
    var movieOverview: MovieOverviewItem? {
        didSet {
            titleLabel.text = movieOverview?.title
            overviewLabel.text = movieOverview?.overview
            
            layoutIfNeeded()
        }
    }
}
