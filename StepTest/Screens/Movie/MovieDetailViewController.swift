import UIKit
import Combine

class MovieDetailViewController: UIViewController {

    private enum Section: Int, CaseIterable {
        case header
        case overview
        case information
    }

    private let viewModel: MovieDetailViewModel
    private var cancellables = Set<AnyCancellable>()
    private var item: MovieDetailItem? {
        didSet {
            DispatchQueue.main.async {
                self.title = self.item?.overview.title
                self.updateBarButtonItem()
                self.collectionView.reloadData()
            }
        }
    }
    
    var completionHandler: ((Int) -> ())?
    
    private lazy var activityIndicatorView = with(UIActivityIndicatorView()) {
        $0.hidesWhenStopped = true
        $0.style = .large
        $0.color = .gray
    }

    private lazy var collectionView = with(UICollectionView(
        frame: .zero,
        collectionViewLayout: makeCollectionViewLayout()
    )) {
        $0.dataSource = self
        $0.backgroundColor = .systemBackground
    }

    public init(viewModel: MovieDetailViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
        setupNavigationView()
        setupViews()
        bindToViewModel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isMovingFromParent {
            guard let item = item else { return }
            completionHandler?(item.id)
        }
    }
    
    private func configureCollectionView() {
        collectionView.register(MovieHeaderCollectionViewCell.self)
        collectionView.register(MovieOverviewCollectionViewCell.self)
        collectionView.register(MovieInformationCollectionViewCell.self)
    }

    private func setupNavigationView() {
        navigationItem.largeTitleDisplayMode = .never
        view.backgroundColor = .systemBackground
    }

    private func setupViews() {
        view.addSubview(collectionView)
        view.addSubview(activityIndicatorView)

        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),

            activityIndicatorView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
    }

    private func bindToViewModel() {
        viewModel.item.sink { [weak self] item in
            self?.item = item
        }.store(in: &cancellables)
        
        viewModel.hasFavourited.sink { [weak self] _ in
            self?.updateBarButtonItem()
        }.store(in: &cancellables)

        viewModel.isLoading.sink { [weak self] in
            self?.configureLoadingState($0)
        }.store(in: &cancellables)
        
        viewModel.hasFailed.sink { [weak self] _ in
            self?.handleFailure()
        }.store(in: &cancellables)

        viewModel.send(.load)
    }

    private func configureLoadingState(_ isLoading: Bool) {
        DispatchQueue.main.async  {
            if isLoading {
                self.activityIndicatorView.startAnimating()
            } else {
                self.activityIndicatorView.stopAnimating()
            }
        }
    }
    
    private func handleFailure() {
        let alertController = UIAlertController(
            title: L10n.Action.errorOccurred,
            message: L10n.Screen.Detail.failureMessagePrompt,
            preferredStyle: .alert
        )

        alertController.addAction(.init(title: L10n.Action.cancel, style: .cancel) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
        alertController.addAction(.init(title: L10n.Action.tryAgain, style: .default) { [weak self] _ in
            self?.viewModel.send(.load)
        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }

    private func makeCollectionViewLayout() -> UICollectionViewLayout {
        return UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment in
            let section = Section(rawValue: sectionIndex)
            
            switch section {
            case .header: return self.makeHeaderLayout()
            case .overview: return self.makeOverviewLayout()
            case .information: return self.makeInformationLayout()
            case .none:
                assert(true, "Unsupported section layout")
                return nil
            }
        }
    }
    
    private func updateBarButtonItem() {
        guard let item = item else { return }

        
        let favouriteBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: item.isFavourited ? "heart.fill" : "heart"),
            style: .plain,
            target: self,
            action: #selector(self.favouriteButtonTapped)
        )
        
        navigationItem.rightBarButtonItem = favouriteBarButtonItem
        navigationItem.rightBarButtonItem?.tintColor = .red
    }
    
    @objc private func favouriteButtonTapped() {
        viewModel.send(.favourite)
    }
}

extension MovieDetailViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Section.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch Section(rawValue: indexPath.section) {
        case .header:
            guard let cell = collectionView.dequeueReusableCell(
                 withReuseIdentifier: MovieHeaderCollectionViewCell.identifier,
                 for: indexPath) as? MovieHeaderCollectionViewCell else {
                     assert(true, "Cannot find MovieHeaderCollectionViewCell")
                     return UICollectionViewCell()
                 }

            cell.url = item?.posterURL
            return cell
        case .overview:
            guard let cell = collectionView.dequeueReusableCell(
                 withReuseIdentifier: MovieOverviewCollectionViewCell.identifier,
                 for: indexPath) as? MovieOverviewCollectionViewCell else {
                     assert(true, "Cannot find MovieOverviewCollectionViewCell")
                     return UICollectionViewCell()
                 }

            cell.movieOverview = item?.overview
            return cell
        case .information:
            guard let cell = collectionView.dequeueReusableCell(
                 withReuseIdentifier: MovieInformationCollectionViewCell.identifier,
                 for: indexPath) as? MovieInformationCollectionViewCell else {
                     assert(true, "Cannot find MovieInformationCollectionViewCell")
                     return UICollectionViewCell()
                 }

            cell.movieInformation = item?.information
            return cell
        default: break
        }
        
        return UICollectionViewCell()
    }
}
    
extension MovieDetailViewController {
    
    private func makeHeaderLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .absolute(160)
        )

        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: itemSize,
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        return section
    }
    
    private func makeOverviewLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .estimated(130)
        )

        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: itemSize,
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 15,
            leading: 15,
            bottom: 0,
            trailing: 15
        )
        
        return section
    }
    
    private func makeInformationLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .estimated(130)
        )

        let item = NSCollectionLayoutItem(layoutSize: itemSize)

        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: itemSize,
            subitem: item,
            count: 1
        )

        let section = NSCollectionLayoutSection(group: group)
        
        section.contentInsets = NSDirectionalEdgeInsets(
            top: 15,
            leading: 15,
            bottom: 0,
            trailing: 15
        )
        
        return section
    }
}
