import Foundation
import Combine

final class MovieDetailViewModel {

    private let network: Network
    private let storage: StorageRepository
    private let id: Int

    let title = PassthroughSubject<String, Never>()
    let item = PassthroughSubject<MovieDetailItem, Never>()
    let hasFavourited = PassthroughSubject<Bool, Never>()
    let isLoading = CurrentValueSubject<Bool, Never>(false)
    let hasFailed = PassthroughSubject<Bool, Never>()

    private var cancellables = Set<AnyCancellable>()

    init(network: Network,
         storage: StorageRepository,
         id: Int
    ) {
        self.network = network
        self.storage = storage
        self.id = id
    }

    public func send(_ event: MovieDetailViewEvent) {
        switch event {
        case .load:
            retrieveItem()
        case .favourite:
            if case let .success(value) = storage.exists(entity: id) {
                if value {
                    storage.delete(entity: id)
                    hasFavourited.send(value)
                } else {
                    storage.create(entity: id)
                    hasFavourited.send(true)
                }
            }
        }
    }

    private func retrieveItem() {
        isLoading.send(true)

        network
            .request(.getMovie(id: id))
            .sink(receiveCompletion: { [weak self] value in
                switch value {
                case .finished: self?.isLoading.send(false)
                case .failure:
                    self?.isLoading.send(false)
                    self?.hasFailed.send(true)
                }
            }, receiveValue: {  [weak self] value in
                self?.item.send(
                    MovieDetailItem(
                        id: value.id,
                        title: value.title,
                        overview: value.overview,
                        releaseDate: value.releaseDate,
                        popularity: value.popularity,
                        posterPath: value.posterPath
                    )
                )
            })
            .store(in: &cancellables)
    }
}

