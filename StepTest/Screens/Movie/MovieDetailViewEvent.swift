public enum MovieDetailViewEvent {
    case load
    case favourite
}
