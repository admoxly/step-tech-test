import Foundation

struct MovieDetailItem {
    let id: Int
    let overview: MovieOverviewItem
    let information: MovieInformationItem
    let posterURL: URL?
    var isFavourited: Bool {
        if case let .success(value) = FavouritesRepository().exists(entity: id) {
            return value
        } else {
            return false
        }
    }
    
    init(id: Int,
         title: String,
         overview: String?,
         releaseDate: Date?,
         popularity: Double,
         posterPath: String?
    ) {
        let mediaURL = URL(string: Environment.themoviedb.mediaURL)
        
        self.id = id
        self.overview = .init(title: title, overview: overview)
        self.information = .init(releaseDate: releaseDate, popularity: popularity)
        self.posterURL = mediaURL?.appendingPathComponent(posterPath ?? "")
    }
}
