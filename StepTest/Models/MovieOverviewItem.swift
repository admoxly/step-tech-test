import Foundation

struct MovieOverviewItem {
    let title: String
    let overview: String?
}
