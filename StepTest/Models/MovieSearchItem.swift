import Foundation

struct MovieSearchItem {
    let id: Int
    let title: String
    
    init(id: Int,
         title: String
    ) {
        self.id = id
        self.title = title
    }
}

extension MovieSearchItem: Equatable {}

extension MovieSearchItem: Hashable {
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
        
    public static func == (lhs: MovieSearchItem, rhs: MovieSearchItem) -> Bool {
        return lhs.id == rhs.id
    }
}
