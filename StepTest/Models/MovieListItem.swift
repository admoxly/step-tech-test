import Foundation

struct MovieListItem {
    let id: Int
    let title: String
    let releaseDate: Date?
    let posterURL: URL?
    var isFavourited: Bool {
        if case let .success(value) = FavouritesRepository().exists(entity: id) {
            return value
        } else {
            return false
        }
    }
    
    init(id: Int,
         title: String,
         releaseDate: Date?,
         posterPath: String?
    ) {
        let mediaURL = URL(string: Environment.themoviedb.mediaURL)
        
        self.id = id
        self.title = title
        self.releaseDate = releaseDate
        self.posterURL = mediaURL?.appendingPathComponent(posterPath ?? "")
    }
}

extension MovieListItem: Equatable {}

extension MovieListItem: Hashable {
    public func hash(into hasher: inout Hasher) {
        return hasher.combine(id)
    }
        
    public static func == (lhs: MovieListItem, rhs: MovieListItem) -> Bool {
        return lhs.id == rhs.id
    }
}
