import Foundation

struct MovieInformationItem {
    let releaseDate: Date?
    let popularity: Double?
}
